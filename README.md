# Faviconator

A Python program that collect as many favicons and logos as possible from a
list of websites.

## Usage

Activate the `nix-shell` using the supplied `default.nix` and then either
supply a file containing the websites to crawl (one per line) or pipe the list
via STDIN:

```bash
$ pdm run python src/faviconator.py websites.csv
$ cat websites.txt | pdm run python src/faviconator.py -
```

### Dependencies

Use `pdm` to install the dependencies from the lockfile:

```bash
$ pdm install
```

Alternatively, it is possible to crate a virtual environment and install the
dependencies via `pip`, not requiring `pdm` at all:

```bash
$ python3 -m venv venv
$ source venv/bin/activate
$ python3 -m pip install .
$ ./src/faviconator.py websites.csv
```

### Verbose output

To enable verbose output, use the `-v/--verbose` CLI flag. This is useful for
debugging.

All logging output goes to STDERR, while the result of the crawler goes to
STDOUT. As an example, it is possible to have the logs written to a file and
the result of the crawler to both STDOUT and a file `results.csv`:

```bash
$ python src/faviconator.py --verbose websites.csv 2>logs | tee results.csv
```

### Output CSV options

By default, the output CSV uses commas to separate the fields. To change this
behavior, use the `-s/--separator`. For example, to use pipes (`|`) instead:

```bash
$ python src/faviconator.py -s "|" websites.csv
```

### Linter and tests

To run the linter and the tests:

```bash
$ pdm run lint
$ pdm run test
```

## TODO

- [x] handle timeouts and make sure the supplied URL list finishes without
  errors
- [x] logging
	- there are no logs so far, add some to STDOUT. Document that with
	  examples of redirecting to a file.
- [ ] fixtures
	- no need to crawl websites on unit tests
- [x] add CLI option to change the CSV separator
- [ ] crawl logos
