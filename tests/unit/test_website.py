"""Test the Website class."""

import requests_mock # noqa

from website import Website


class TestWebsite:
    """Test the Website class."""

    # tests for building the object
    # TODO mock the requests here, no need to actually try to get the website
    def test_url_without_scheme(self):
        """Test we can set the URL of the website."""
        url = "example.com"
        website = Website(url)
        assert website.url == "https://" + url

    def test_url_with_http(self):
        """Test we can set the URL of the website."""
        url = "http://example.com"
        website = Website(url)
        assert website.url == url

    def test_url_with_https(self):
        """Test we can set the URL of the website."""
        url = "https://example.com"
        website = Website(url)
        assert website.url == url

    # tests for favicon
    def test_mocked_favicon(self, requests_mock): # noqa
        """Test we can extract a favicon from the HTML."""
        url = "https://example.com"
        mocked_html = """
 <!DOCTYPE html>
<html>
<head>
  <title>Mocked website</title>
  <link rel="icon" type="image/x-icon" href="/images/favicon.ico">
</head>
<body>
</body>
</html>
        """
        requests_mock.get(url, text=mocked_html)

        website = Website(url)
        assert website.favicon == "/images/favicon.ico"
