"""Final integration tests.

Test if the system can actually query the websites and get their information.
"""

import pytest

from website import Website


def test_nonurl():
    """Test a bad url does not crash the system."""
    website = Website("invalid-url")
    assert website.favicon is None


@pytest.mark.parametrize("url,expected",
                         [("heitorpb.github.io", "/assets/favicon.png"),
                          ("facebook.com", "https://static.xx.fbcdn.net/rsrc.php/yv/r/B8BxsscfVBr.ico"), # noqa
                          ("mixrank.com", "/static/images/favicon.ico"),
                          ])
def test_favicons(url, expected):
    """Test we can get the favicons for some websites."""
    website = Website(url)
    assert website.favicon == expected
