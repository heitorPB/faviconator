"""Website class."""
import logging

from bs4 import BeautifulSoup
import requests


logger = logging.getLogger(__name__)


class Website:
    """Website class."""

    def __init__(self, url: str = ""):
        """Initialize object."""
        self._url = url
        self._default_scheme = "https://"
        self._html = None
        self._soup = None

        if url:
            self.crawl(url)

    def crawl(self, url: str):
        """Crawl website and store it's HTML."""
        self._download_page()
        self._cook_soup()

    def _cook_soup(self):
        """Parse HTML and generate Soup object."""
        if self._html:
            logger.debug("Parsing HTML into soup object")
            self._soup = BeautifulSoup(self._html, 'html5lib')
        else:
            logger.warning("No HTML to parse into soup object")

    def _download_page(self):
        """Download HTML and store it."""
        try:
            logger.debug("Downloading HTML for %s", self.url)
            self._html = requests.get(self.url, timeout=5).text
        except (requests.exceptions.ConnectTimeout,
                requests.exceptions.ReadTimeout) as e:
            logger.error("Connection timeout: %s", e)
            self._html = None
        except requests.exceptions.ConnectionError as e:
            logger.error("Connection error: %s", e)
            self._html = None
        except requests.exceptions.ContentDecodingError as e:
            logger.error("Decoding error: %s", e)
            # TODO we could re-try with the suggestion from
            # https://github.com/psf/requests/issues/3849
            self._html = None

    @property
    def url(self) -> str:
        """Return the URL of the website."""
        if "http" in self._url:
            return self._url
        else:
            return self._default_scheme + self._url

    @property
    def favicon(self) -> str:
        """Return the URL of the website's favicon.

        Returns the URL (as a string) to the favicon or None if it was not
        found.

        Limitations:
        - does not return the full URL to the favicon in some cases. Some
          websites use a relative path instead and it is this value that is
          returned.
        - some websites embbed the image in base64, in these cases the returned
          data is not the URL but the encoded icon.
        """
        if not self._soup:
            logger.warning("No soup object to extract favicon")
            return None

        logger.debug("Parsing soup object to get the favicon")
        head = self._soup.head
        icon_links = head.find_all("link", rel="icon")

        if len(icon_links) > 0:
            logger.debug("Favicon found")
            return icon_links[0]["href"]
        else:
            logger.debug("Favicon not found in soup object")
            return None

    @property
    def logo(self) -> str:
        """Return the URL of the website's logo."""
        pass
