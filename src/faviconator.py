"""CLI to crawl websites for their favicons and logos."""
import logging

import click

from website import Website


@click.command()
@click.option("-s", "--separator", default=",", show_default=True,
              help="Separator used for the resulting CSV.")
@click.option("-v", "--verbose", is_flag=True, default=False,
              help="Show debugging messages about internal operations.")
@click.argument("urls", type=click.File("rb"))
def crawl(verbose, separator, urls):
    """Crawl websites and get their favicons and logos.

    URLS is the file containing a list of websites to crawl, one on each line.
    If set to -, read from stdin until an EOF is found.
    """
    if verbose:
        level = logging.DEBUG
    else:
        level = logging.CRITICAL
    logging.basicConfig(level=level)

    while True:
        url = urls.readline().decode().strip()
        if not url:
            # end of file or EOF
            break

        web = Website(url)
        click.echo(f"{web.url}{separator}{web.favicon}{separator}{web.logo}")


if __name__ == "__main__":
    crawl()
